# This script extract global context vector and original vectors from the folder '../final_adaptive_kernel' which is the model that we used in the ral submission.
from __future__ import print_function, division
import sys, time, glob, os, ntpath, numpy
from PIL import Image
import torchvision.transforms as transforms
import torch 
import torch.nn as nn
import torch.optim as optim
import torchvision.datasets as dsets
import torchvision.transforms as transforms
from torch.autograd import Variable
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
import time
import copy
import os
import torch.nn.functional as F
import random
import scipy.io as sio
from scipy.spatial.distance import cdist
from torch.nn.parameter import Parameter
import math

def normlize_mat(mat): # to normalize each row, so that the min value of each row is zero and they add up to one
    # mat: a m*n matrix
    #print(mat.size())
    value,ind = torch.min(mat,0) # 'value': the min value of each row
    min_value = value.expand_as(mat)
    newmat = mat + min_value # now the min value of each row is 0
    summat = (newmat.sum(0)).expand_as(mat) 
    finalmat = newmat/summat # how to avoid divided by 0
    return finalmat



dtype = torch.FloatTensor

class Context_cosine(nn.Module):
    r"""
	Apply cosine similarity to measure feature relevance when constructing context vector;	
	Applies a linear transformation to the incoming data: :math:`y = Ax + b`
    Args:
        in_features: size of each input sample
        out_features: size of each output sample
        bias: If set to False, the layer will not learn an additive bias.
            Default: True
    Shape:
        - Input: :math:`(N, *, in\_features)` where `*` means any number of
          additional dimensions
        - Output: :math:`(N, *, out\_features)` where all but the last dimension
          are the same shape as the input.
    Attributes:
        weight: the learnable weights of the module of shape
            (out_features x in_features)
        bias:   the learnable bias of the module of shape (out_features)
    Examples::
        >>> m = nn.Linear(20, 30)
        >>> input = autograd.Variable(torch.randn(128, 20))
        >>> output = m(input)
        >>> print(output.size())
    """

    def __init__(self, in_features, out_features, bias=False):
        super(Context_cosine, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.Tensor(out_features, in_features))
        #if bias:
        #    self.bias = Parameter(torch.Tensor(out_features))
        #else:
        #    self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self): # normalize the weight
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        #if self.bias is not None:
         #   self.bias.data.uniform_(-stdv, stdv)

    def forward(self, context,feat): # context: tensor of context vectors,size(64,512,784); feat: tensor of local descriptors,size(64,512,196)
	#self.weight.data.clamp_(min=0)
	#print(self.weight)
        #return F.linear(input, self.weight,self.bias)
	temp = context.permute(0,2,1) # (64,784,512)
	temp = temp.contiguous() #(64,784,512)
	temp_view = temp.view(-1,temp.size(2)) # shape (64*784,512)
	temp_mul = torch.mm(temp_view,self.weight) # shape (64*784,512)
	temp_reshape = temp_mul.view(temp.size(0),temp.size(1),-1) # (64,784,512) 
	relevance = torch.bmm(temp_reshape,feat) # (64,784,196) # each column measures relevance scores for one of the 196 local features
	return torch.bmm(context,relevance) # (64,512,196) # the context feat that can be used for calculate 
	
	

    def __repr__(self):
        return self.__class__.__name__ + ' (' \
            + str(self.in_features) + ' -> ' \
            + str(self.out_features) + ')'


class BasicConv2d(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size,stride,padding):
        super(BasicConv2d, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size,stride,padding)
        self.bn = nn.BatchNorm2d(out_channels, eps=0.001)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
	return F.relu(x, inplace=True)

class BasicConv2d_nobn(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size,stride,padding):
        super(BasicConv2d_nobn, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size,stride,padding)

    def forward(self, x):
        x = self.conv(x)
	return F.relu(x, inplace=True)

# Generate Context feature from a convolutional feature map
	

class MyModule(nn.Module):
    def __init__(self,original_module):
        super(MyModule,self).__init__()
        self.layer1 = nn.Sequential(*list(original_module.features.children())[:-5])
        self.layer2 = nn.Sequential(*list(original_module.features.children())[-5:-3])
	self.layer3 = nn.Sequential(*list(original_module.features.children())[-3:-2]) # last layer without relu

	self.atten_mul1 = Context_cosine(512,512) # 512 is the dimensionality of local features, if the dim changes, then 512 should change
	self.atten_mul2 = Context_cosine(512,512)
	self.atten_mul3 = Context_cosine(512,512)

	self.sigmoid = nn.Sigmoid()

	self.normalize = normlize_mat
	self.context_to_mask1 = BasicConv2d(512,1,1,1,0)
	self.context_to_mask2 = BasicConv2d(512,1,1,1,0)
	self.context_to_mask3 = BasicConv2d(512,1,1,1,0)

	self.mergemask = BasicConv2d_nobn(3,1,1,1,0)

	self.layer12_conv = BasicConv2d(512,256,3,2,0)
	self.Batch1 = nn.BatchNorm2d(512,eps=0.001)
	self.Batch2 = nn.BatchNorm2d(512,eps=0.001)
	self.Batch3 = nn.BatchNorm2d(512,eps=0.001)
	
	self.fc1 = nn.Linear(9216,1136)
	self.cat = torch.cat
	#self.bn = nn.BatchNorm1d(1501,eps=0.001)
	#self.bn2 = nn.BatchNorm1d(3136,eps=0.001)

    def forward(self,x):
	layer1_out_ori = self.layer1(x)
	layer1_out = self.Batch1(layer1_out_ori)
	#print('layer1 shape is')
	#print(layer1_out.size()) # shape is (64,512,14,14). 64 is the batch size

	layer2_out_ori = self.layer2(layer1_out_ori)
	layer2_out = self.Batch2(layer2_out_ori)
	#print('layer2 shape is')
	#print(layer2_out.size()) # shape is (64,512,14,14)

	layer3_out_ori = self.layer3(layer2_out_ori)
	layer3_out = self.Batch3(layer3_out_ori)
	layer3_view = layer3_out.view(layer3_out.size(0),layer3_out.size(1),-1) # shape (64,512,196)
	#print('layer 3 shape is')
	#print(layer3_out.size()) # shape is (64,512,14,14) # 64 is the batch size

	layer1_view = layer1_out.view(layer1_out.size(0),layer1_out.size(1),-1) # shape (64,512,196)
	layer1_context_view = self.atten_mul1(layer1_view,layer1_view)  # now in shape (64,512,196) this is the context feature for layer1, each row is a context feature
	layer1_context_view = layer1_context_view.view(layer1_context_view.size(0),layer1_context_view.size(1),14,14) # shape(64,512,14,14)

	#print(self.atten_mul3.weight)
        
	layer2_view = layer2_out.view(layer2_out.size(0),layer2_out.size(1),-1) # shape (64,512,196)
	layer2_context_view = self.atten_mul2(layer2_view,layer2_view) # now (64,512,196)
	layer2_context_view = layer2_context_view.view(layer2_context_view.size(0),layer2_context_view.size(1),layer2_out.size(2),layer2_out.size(3)) # shape(64,512,14,14)
	

	
	layer3_context_view = self.atten_mul3(layer3_view,layer3_view) # now (64,512,196) 
	layer3_context_view = layer3_context_view.view(layer3_context_view.size(0),layer3_context_view.size(1),layer3_out.size(2),layer3_out.size(3)) # shape(64,512,14,14)

	l13 = layer1_context_view + layer1_out # shape (64,512,14,14)
	l23 = layer2_context_view + layer2_out
	l33 = layer3_context_view + layer3_out

	l13_mask = self.context_to_mask1(l13) # shape (64,1,14,14)	
	#print('shape 1 is')
	#print(l13_mask.size())
	l23_mask = self.context_to_mask2(l23)
	l33_mask = self.context_to_mask3(l33) # shape (64,1,14,14) # l33 is non-zero while, l33_mask is all zero, the weights are normal. very strange!
	#print(l33_mask)

	merge_mask = self.cat([l13_mask,l23_mask,l33_mask],1) # shape (64,3,14,14)
	#print('merge shape is')
	#print(merge_mask.size())
	
	final_mask = self.mergemask(merge_mask) # shape (64,1,14,14)# learn the weight of different scales

	#final_mask = self.sigmoid(one_mask) # shape (64,1,14,14) rescale to [0,1] # remove sigmoid, because 0 is changed to 0.5 in sigmoid

	extend_mask = final_mask.expand_as(layer3_out) # shape (64,512,14,14)

	layer_mask = extend_mask * layer3_out # shape (64,512,14,14)
	
	what = self.layer12_conv(layer_mask) # (64, 256,6,6)

	l1_l2_flat = what.view(what.size(0),-1) # (64, 9216)

	fc = self.fc1(l1_l2_flat)

	return layer_mask.squeeze(1)


model = models.vgg16(pretrained = True)
#cnn.classifier = nn.Sequential(nn.Linear(25088,1136));

cnn = MyModule(model) # %%%%%%%%%%%%%%%% change module, then change here
cnn.load_state_dict(torch.load('pkl/cnn_attention.pkl'))

print('after')
print(cnn)

cnn.cuda()

cnn.eval();

normalize = transforms.Normalize(
   mean=[0.485, 0.456, 0.406],
   std=[0.229, 0.224, 0.225]
)

preprocess = transforms.Compose([
   transforms.Scale(256),
   transforms.CenterCrop(224),
   transforms.ToTensor(),
   normalize
])

def getFeaturesFromDir(dirName1):
    types = ('*.jpg', '*.JPG', '*.png')    
    imageFilesList1 = [] # as reference dataset
    for files in types:
        imageFilesList1.extend(glob.glob(os.path.join(dirName1, files)))
    imageFilesList1 = sorted(imageFilesList1)

    i1_start = 0
    cut1 = 0

    for i, imFile in enumerate(imageFilesList1):  # for each reference image   
        print(imFile)
        img = Image.open(imFile)
        img_tensor = preprocess(img)
        img_tensor.unsqueeze_(0) # after this, img_tensor's shape changes from [3*224*224] to [1*3*224*224]
	images = Variable(img_tensor).cuda()
	finalmask = cnn(images) ## extract features here
        
	if cut1 == 0:
            allfinal = finalmask.data ## concatenate featues here
        else:
            allfinal = torch.cat([allfinal,finalmask.data])
	cut1 = cut1 + 1
        print(allfinal.size())

    allfinal = allfinal.cpu().numpy()
	
    return allfinal,imageFilesList1



dataset = sys.argv[1]
fullpath1 = '/home/zetao/Projects/dataset/' + dataset # images are saved under this folder, images are named in acending order
print('Path 1 is  ')
print(fullpath1)

m_final,filelist = getFeaturesFromDir(fullpath1)


print('saving feature matrix')
savepath = 'feat_mat/feat_' + dataset + '.mat'

sio.savemat(savepath,{'finalmask':m_final,'filelist':filelist})


