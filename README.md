# Learning Context Flexible Attention Model for Long-term Visual Place Recognition
This repository contains code for our IROS 2017 paper "[Learning Context Flexible Attention Model for Long-Term Visual Place Recognition](https://ieeexplore.ieee.org/abstract/document/8421024)" by Zetao Chen, Lingqiao Liu, Inkyu Sa, Zongyuan Ge and Margarita Chli.
![attention](attention_images/attention.png)
## Setup
- Linux
- Install Pytorch from [here](https://pytorch.org/). Note this implementation is tested on python2.7 and cuda 8.0. 

## Steps:
- In the file 'extract_feat.py', update the line 'fullpath1 = '/home/zetao/Projects/dataset/' + dataset' with the path of your own dataset

- Run the script 'extract_feat.sh'; 
